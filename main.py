###################################
# Author: Matias Lappi
# Program name: BlackJackPython
###################################
import random
import os
from typing import List

class Card:
    suit: str
    rank: str
    value: int

    def __init__(self, suit: str, rank: str, value: int):
        self.suit = suit
        self.rank = rank
        self.value = value

class Deck:
    deck: List[Card]

    def __init__(self):
        self.deck = []
        self.generate_cards()
        self.shuffle()

    def generate_cards(self) -> None:
        suits = ["♠", "♥", "♣", "♦"]
        ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
        for suit in suits:
            for rank in ranks:
                value = ranks.index(rank) + 2
                if rank in ["J", "Q", "K"]:
                    value = 10
                elif rank == "A":
                    value = 11
                self.deck.append(Card(suit, rank, value))

    def shuffle(self) -> None:
        random.shuffle(self.deck)

    def deal_card(self) -> Card:
        return self.deck.pop()

    def reset_deck(self) -> None:
        self.deck = []
        self.generate_cards()
        self.shuffle()

class Dealer:
    hand: List[Card]
    hand_value: int
    aces: int

    def __init__(self):
        self.hand = []
        self.hand_value = 0
        self.aces = 0

    def add_card_to_hand(self, card: Card) -> None:
        self.hand.append(card)
        self.hand_value += card.value
        if card.rank == "A":
            self.aces += 1
        if self.hand_value > 21 and self.aces > 0:
            self.hand_value -= 10
            self.aces -= 1

    def print_hand(self) -> None:
        card_art = [
            "╭─────────╮",
            "│{:<2}       │",
            "│         │",
            "│    {}    │",
            "│         │",
            "│       {:>2}│",
            "╰─────────╯"
        ]
        for item in card_art:
            for card in self.hand:
                if card_art.index(item) == 3:
                    print(item.format(card.suit), end="")
                else:
                    print(item.format(card.rank), end="")
            print("")
        print(" = {}\n".format(self.hand_value))

    def clear_hand(self) -> None:
        self.hand = []
        self.hand_value = 0
        self.aces = 0

class Player(Dealer):
    bet: int
    chips: int

    def __init__(self):
        Dealer.__init__(self)
        self.bet = 0
        self.chips = 0
        self.choose_amount_of_chips()

    def choose_amount_of_chips(self) -> None:
        while True:
            try:
                chips = int(input("Choose amount of chips to play with: "))
                if chips < 2:
                    print("Minimun debosit is 2.")
                else:
                    self.chips = chips
                    break
            except ValueError:
                print("Input a positive integer.")

    def place_bet(self) -> None:
        print("Your chips: {}".format(self.chips))
        while True:
            try:
                self.bet = int(input("Your bet: "))
                if self.bet > self.chips:
                    print("You don't have enough chips.")
                elif self.bet < 2:
                    print("Minimum bet is 2 chips.")
                else:
                    self.chips -= self.bet
                    break
            except ValueError:
                print("Input a positive integer.")

    def menu(self) -> int:
        while True:
            print("1) Hit, 2) Double, 0) Stand")
            try:
                choice = int(input("Your choice: "))
                if choice == 2 and self.bet > self.chips:
                    print("You don't have enough chips to double.")
                else:
                    return(choice)
            except ValueError:
                pass

def print_game(dealer: Dealer, player: Player) -> None:
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Dealer's cards:")
    dealer.print_hand()
    print("Your cards:")
    player.print_hand()
    print("Your bet: {}".format(player.bet))
    print("Your chips: {}".format(player.chips))

def play_round(dealer: Dealer, player: Player, deck: Deck) -> None:
    player.place_bet()

    dealer.add_card_to_hand(deck.deal_card())
    for i in range(2):
        player.add_card_to_hand(deck.deal_card())

    if player.hand_value == 21:
        print_game(dealer, player)
        print("You got a BlackJack!")
        player.chips += player.bet * 2.5

    else:
        while player.hand_value <= 21:
            print_game(dealer, player)
            choice = player.menu()
            if choice == 0:
                break
            elif choice == 1:
                player.add_card_to_hand(deck.deal_card())
            elif choice == 2:
                player.add_card_to_hand(deck.deal_card())
                player.chips -= player.bet
                player.bet *= 2
                break

        if player.hand_value > 21:
            print_game(dealer, player)
            print("You busted.")

        else:
            while dealer.hand_value < 17:
                dealer.add_card_to_hand(deck.deal_card())
            if dealer.hand_value > 21:
                print_game(dealer, player)
                print("The dealer busted.")
                player.chips += player.bet * 2
            elif dealer.hand_value == player.hand_value:
                print_game(dealer, player)
                print("Push.")
                player.chips += player.bet
            elif dealer.hand_value > player.hand_value:
                print_game(dealer, player)
                print("Dealer won.")
            elif dealer.hand_value < player.hand_value:
                print_game(dealer, player)
                print("You won!")
                player.chips += player.bet * 2

    player.clear_hand()
    dealer.clear_hand()
    deck.reset_deck()

def initGame() -> None:
    dealer = Dealer()
    player = Player()
    deck = Deck()

    while player.chips >= 2:
        play_round(dealer, player, deck)

    print("You ran out of chips.")
    print("Closing the game...")

if __name__ == "__main__":
    initGame()
